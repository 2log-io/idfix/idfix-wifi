/*   2log.io
 *   Copyright (C) 2021 - 2log.io | mail@2log.io,  sascha@2log.io
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "WiFiUtils.h"

extern "C"
{
	#include "esp_wifi_types.h"
	#include "esp_netif.h"
}

namespace IDFix
{
	namespace WiFi
	{
        const char *WiFiUtils::wiFiEventTypeToString(int32_t eventType)
        {
            switch (eventType)
            {
                case WIFI_EVENT_WIFI_READY:				return "WIFI_EVENT_WIFI_READY";
                case WIFI_EVENT_SCAN_DONE:				return "WIFI_EVENT_SCAN_DONE";
                case WIFI_EVENT_STA_START:				return "WIFI_EVENT_STA_START";
                case WIFI_EVENT_STA_STOP:				return "WIFI_EVENT_STA_STOP";
                case WIFI_EVENT_STA_CONNECTED:			return "WIFI_EVENT_STA_CONNECTED";
                case WIFI_EVENT_STA_DISCONNECTED:		return "WIFI_EVENT_STA_DISCONNECTED";
                case WIFI_EVENT_STA_AUTHMODE_CHANGE:	return "WIFI_EVENT_STA_AUTHMODE_CHANGE";
                case WIFI_EVENT_STA_WPS_ER_SUCCESS:		return "WIFI_EVENT_STA_WPS_ER_SUCCESS";
                case WIFI_EVENT_STA_WPS_ER_FAILED:		return "WIFI_EVENT_STA_WPS_ER_FAILED";
                case WIFI_EVENT_STA_WPS_ER_TIMEOUT:		return "WIFI_EVENT_STA_WPS_ER_TIMEOUT";
                case WIFI_EVENT_STA_WPS_ER_PIN:			return "WIFI_EVENT_STA_WPS_ER_PIN";
                case WIFI_EVENT_AP_START:				return "WIFI_EVENT_AP_START";
                case WIFI_EVENT_AP_STOP:				return "WIFI_EVENT_AP_STOP";
                case WIFI_EVENT_AP_STACONNECTED:		return "WIFI_EVENT_AP_STACONNECTED";
                case WIFI_EVENT_AP_STADISCONNECTED:		return "WIFI_EVENT_AP_STADISCONNECTED";
                case WIFI_EVENT_AP_PROBEREQRECVED:		return "WIFI_EVENT_AP_PROBEREQRECVED";
                #ifdef CONFIG_IDF_TARGET_ESP32
                    case WIFI_EVENT_STA_WPS_ER_PBC_OVERLAP:	return "WIFI_EVENT_STA_WPS_ER_PBC_OVERLAP";
                    case WIFI_EVENT_MAX:					return "WIFI_EVENT_MAX";
                #endif
            }

            return "NULL";
        } 
 
        const char *WiFiUtils::wifiDisconnectReasonToString(uint8_t reasonCode)
        { 
            switch (reasonCode)
            { 
                case WIFI_REASON_UNSPECIFIED:               return "WIFI_REASON_UNSPECIFIED";
                case WIFI_REASON_AUTH_EXPIRE:               return "WIFI_REASON_AUTH_EXPIRE";
                case WIFI_REASON_AUTH_LEAVE:                return "WIFI_REASON_AUTH_LEAVE";
                case WIFI_REASON_ASSOC_EXPIRE:              return "WIFI_REASON_ASSOC_EXPIRE";
                case WIFI_REASON_ASSOC_TOOMANY:             return "WIFI_REASON_ASSOC_TOOMANY";
                case WIFI_REASON_NOT_AUTHED:                return  "WIFI_REASON_NOT_AUTHED";
                case WIFI_REASON_NOT_ASSOCED:               return  "WIFI_REASON_NOT_ASSOCED";
                case WIFI_REASON_ASSOC_LEAVE:               return  "WIFI_REASON_ASSOC_LEAVE";
                case WIFI_REASON_ASSOC_NOT_AUTHED:          return "WIFI_REASON_ASSOC_NOT_AUTHED";
                case WIFI_REASON_DISASSOC_PWRCAP_BAD:       return  "WIFI_REASON_DISASSOC_PWRCAP_BAD";
                case WIFI_REASON_DISASSOC_SUPCHAN_BAD:      return  "WIFI_REASON_DISASSOC_SUPCHAN_BAD";
                case WIFI_REASON_IE_INVALID:                return  "WIFI_REASON_IE_INVALID";
                case WIFI_REASON_MIC_FAILURE:               return  "WIFI_REASON_MIC_FAILURE";
                case WIFI_REASON_4WAY_HANDSHAKE_TIMEOUT:    return "WIFI_REASON_4WAY_HANDSHAKE_TIMEOUT";
                case WIFI_REASON_GROUP_KEY_UPDATE_TIMEOUT:  return "WIFI_REASON_GROUP_KEY_UPDATE_TIMEOUT";
                case WIFI_REASON_IE_IN_4WAY_DIFFERS:        return  "WIFI_REASON_IE_IN_4WAY_DIFFERS";
                case WIFI_REASON_GROUP_CIPHER_INVALID:      return  "WIFI_REASON_GROUP_CIPHER_INVALID";
                case WIFI_REASON_PAIRWISE_CIPHER_INVALID:   return  "WIFI_REASON_PAIRWISE_CIPHER_INVALID";
                case WIFI_REASON_AKMP_INVALID:              return  "WIFI_REASON_AKMP_INVALID";
                case WIFI_REASON_UNSUPP_RSN_IE_VERSION:     return "WIFI_REASON_UNSUPP_RSN_IE_VERSION";
                case WIFI_REASON_INVALID_RSN_IE_CAP:         return  "WIFI_REASON_INVALID_RSN_IE_CAP";
                case WIFI_REASON_802_1X_AUTH_FAILED:         return  "WIFI_REASON_802_1X_AUTH_FAILED";
                case WIFI_REASON_CIPHER_SUITE_REJECTED:      return  "WIFI_REASON_CIPHER_SUITE_REJECTED";
 
                case WIFI_REASON_BEACON_TIMEOUT:             return  "WIFI_REASON_BEACON_TIMEOUT";
                case WIFI_REASON_NO_AP_FOUND:                return  "WIFI_REASON_NO_AP_FOUND";
                case WIFI_REASON_AUTH_FAIL:                  return  "WIFI_REASON_AUTH_FAIL";
                case WIFI_REASON_ASSOC_FAIL:                 return  "WIFI_REASON_ASSOC_FAIL";
                case WIFI_REASON_HANDSHAKE_TIMEOUT:          return  "WIFI_REASON_HANDSHAKE_TIMEOUT";
            } 
            return "NULL";
        }

		const char *WiFiUtils::ipEventTypeToString(int32_t eventType)
		{
			switch (eventType)
			{
				case IP_EVENT_STA_GOT_IP:			return "IP_EVENT_STA_GOT_IP";
				case IP_EVENT_STA_LOST_IP:			return "IP_EVENT_STA_LOST_IP";
				case IP_EVENT_AP_STAIPASSIGNED:		return "IP_EVENT_AP_STAIPASSIGNED";
				case IP_EVENT_GOT_IP6:				return "IP_EVENT_GOT_IP6";
                #ifdef CONFIG_IDF_TARGET_ESP32
                    case IP_EVENT_ETH_GOT_IP:			return "IP_EVENT_ETH_GOT_IP";
                #endif
			}

			return "NULL";
		}
	}
}
